#!/usr/bin/env bash
echo "Stoping contextnet"
killall java
echo "Removing old file"
rm ~/Downloads/SDDLServer-1.0-SNAPSHOT-jar-with-dependencies.jar
git pull 
echo "Copying new file"
cp SDDLServer-1.0-SNAPSHOT-jar-with-dependencies.jar ~/Downloads
cd ~/Downloads
echo "Starting contextnet"
java -jar contextnet-2.5.jar 120.0.1 5500 OpenSplice &
sleep 1.8
echo "Starting server"
java -jar SDDLServer-1.0-SNAPSHOT-jar-with-dependencies.jar 
